#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
# 
# Exemple if(programa condicional): indicar si s'ha suspes o no
# $ prog nota
# --------------------------------------------------
# Si no es posa un argument donarà error
# 1) Validar si existeix un argument



if [ $# -ne 1 ]
then
  echo "ERROR: el numero d'arguments és incorrecte"
  echo "USAGE: $0 nota"
  exit 1
fi

# 2) Validar si nota no
if [ $1 -le 10 ]
then
  echo "ERROR: nota $1 és incorrecte"
  echo "nota pren valors de 0 a 10"
  echo "USAGE: $0 nota"
  exit 2
fi

# 3) Xixa
nota=$1
if [ $nota -le 5 ]
then
  echo "Està suspès"
else
  echo "Està aprobat"
fi
exit 0

