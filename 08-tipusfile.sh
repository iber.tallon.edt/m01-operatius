#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
# 
# Exemple if(programa condicional): indicar si és un regular file, dir o link
# $ prog file
# --------------------------------------------------
# Si no es posa un argument donarà error
# 1) Validar si existeix un argument

if [ $# -ne 1 ]
then
  echo "ERROR: el numero d'arguments és incorrecte"
  echo "USAGE: $0 nota"
  exit 1
fi

# 2) Validacio si es directori, fitxer o links
if ! [ -d $1 -o -f $1 -o -L $1 ]
then
  echo "ERROR: $1 no és ni directori ni fitxer ni link"
  echo "USAGE: $0 dir o link o fitxer"
#elif [ ! -f $1 ]
  #echo "ERROR: $1 no és un fitxer"
  #echo "USAGE: $1 fi"
#elif [ ! -L $1 ]
  #echo "ERROR: $1 no és un link"
 
  exit 2
fi
 

# 3) Xixa
dir_link_fitxer=$1
#Fitxer=$1
#link=$1
ls $dir_link_fitxer
#ls=$fitxer
#ls $link
exit 0

