#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
# 
# Exemple if(programa condicional): indicar un nom i la seva edat i mostrar-los
# $ prog nom i edat
# --------------------------------------------------
# Si no es posan dos arguments donarà error
# 1) Validar si existeix un argument
if [ $# -ne 2 ]
then
  echo "ERROR: el numero d'arguments és incorrecte"
  echo "USAGE: $0 nom edat"
  exit 1
fi

# 2) Xixa
echo "nom: $1"
echo "edat: $2"
exit 0

