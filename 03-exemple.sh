#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
# 
# Exemple if(programa condicional): indicar si és major d'edat
# $ prog edat
# --------------------------------------------------
# En cada pas que es fagi, s'ha d'anar probant.
# Sempre s'ha de posar noms de varaibles significatius amb el que vulguis trobar o mostrar.
# La sintaxis de les variables han de ser llista_noms(noms=el que vulguis) o llistaNoms.
# La sintaxis de les constants 
# Sempre que es posi un limit s'ha de probar el limit, el mes gran que el limit i el mes petit que el limit.
# Sempra s'ha de fer un programa ordenadament. Ex: dien
# 1) Validar que existeix un argument. Primer bloc que diu si has rebut un argument o no.
if [ $# -ne 1 ]
then
  echo "Error: numero args incorrecte"
  echo "Usage: $0 edat"
  exit 1
fi
# Si no es troba un argument s'ha de mostrar(error d'esqüalificat):
	# missatge d'error
	# missatge usage
	# exit 1

# 2) Xixa. Segon bloc que fa el programa amb l'argument introduit.
edat=$1
if [ $edat -lt 18 ]
# en [] sempre s'ha de pusar espai
# -ge = més gran o igual
then
  echo "edat $edat és menor d'edat"
elif [ $edat -lt 65 ]
then
  echo "edat $edat és de població activa"
# S'ha posat $edat i no $1 per saber del que s'esta parlant i saber el mostrarà.
else
  echo "edat $edat és jubilat"
fi
exit 0

# [ $# -ne 1 ] -> Unicament un argument

# ex:
# bash 03-exemple.sh 20
# edat 20 és major d'edat

# if [ condicio ]
	# Les condicions
	# 0 -> Bon funcionament del programa
	# Diferent a 0 -> Errors

# Operadors relacionals (=, !=, >, <, >=, <=). Es un operador binari. 
# -eq -> = 
# -lt -> 
# Operadors strings/cadenes
# 
# Operadors logics (and, or, not). Es un operador unari.
# -a -> and
# -o -> or
# ! -> not

# Ex de operadors
	# test 10 -lt 15; echo $?
	# 0
	# test 15 -ge 16; echo $?
	# 1
	# [ $num -gt 1 ]; echo $?
	# 0
	# 

# [ arg -ge(més gran o igual a 1) 1 -a(and) arg -lt(més petit o igual a 18) 18 ]

# if and else
# if [cond]
# then
#   xixa
# else
#   xixa
# fi


# if, elif and else
# if [cond]
# then
#   xixa
# elif [cond]
#   xixa
# else
#   xixa
# fi

