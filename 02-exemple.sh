#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
# 
# Exemple de processar arguments(programa sequencial)
# --------------------------------------------------
echo '$*: ' $*
echo '$@: ' $@
echo '$#: ' $#
echo '$0: ' $0
echo '$1: ' $1
echo '$2: ' $2
echo '$9: ' $9
echo '$10: ' ${10}
echo '$11: ' ${11}
echo '$$: ' $$
# $* -> argument 
# $@ -> llista
# $# -> El numero d'arguments
# $* ->  
# $$ -> pid del programa
# $0 -> nom del programa. NO forma part de la llista d'arguments
# $1 -> mostra primer argument
# $2 -> mostra segon argument
# $9 -> mostra nové argument
# ${10} -> mostra decé argument. {} es posa quan hi ha més d'un digit 
# ${11} -> mostra primer argument


# ex:
# bash 02-exemple.sh aa bb cc dd ee ff gg hh ii jj kk ll 
# nom del programa = 02-exemple.sh
# arguments = aa, bb, cc, dd, ee, ff, gg, hh, ii, jj, kk, ll. Cada un és un argument

nom="puig"
echo"${nom}deworld"
