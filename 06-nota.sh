#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
# 
# Exemple if(programa condicional): indicar si s'ha suspes, aprovat, notable o 
# exelent 
# $ prog nota
# --------------------------------------------------
# Si no es posa un argument donarà error
# 1) Validar si existeix un argument

if [ $# -ne 1 ]
then
  echo "ERROR: el numero d'arguments és incorrecte"
  echo "USAGE: $0 nota"
  exit 1
fi

# 2) Validar rang nota
if [ $1 -gt 10 ]
then
  echo "ERROR: nota $1 és incorrecte"
  echo "nota pren valors de 0 a 10"
  echo "USAGE: $0 nota"
  exit 2
fi

# 3) Xixa
nota=$1
if [ $nota -le 5 ]
then
  echo "Està suspès"
elif [ $nota -lt 7 ]
  echo "Està aprovat"
elif [ $nota -lt 9 ]
  echo "Té un notable"
else
  echo "Té un exelent"
fi
exit 0

