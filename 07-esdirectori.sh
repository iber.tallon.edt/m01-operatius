#! /bin/bash
# @edt ASIX-M01
# Febrer 2023
# 
# Exemple if(programa condicional): indicar si és un directori 
# $ prog dir
# --------------------------------------------------
# Si no es posa un argument donarà error
# 1) Validar si existeix un argument

if [ $# -ne 1 ]
then
  echo "ERROR: el numero d'arguments és incorrecte"
  echo "USAGE: $0 nota"
  exit 1
fi

# 2) Validar si es directori
if [ ! -d $1 ]
then
  echo "ERROR: $1 no es un directori"
  echo "USAGE: $0 dir"
  exit 2
fi

# 3) Xixa
dir=$1
ls $dir
exit 0

